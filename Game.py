from random import *
from math import floor, ceil
import numpy as np
import collections

class Game(object):

    PLUSATOM = -1
    BLACKPLUS = -3
    DEL = -2
    def __init__(self):
        self.board = []
        self.nextElem = 0
        self.score = 0
        self.lastPlus=0
        self.lastminus=0
        self.movecount=0
        self.maxAtom = 3
        self.minAtom = 1
        self.size = 0
        self.initBoard()
    
    def step(self, index):
        self.movecount += 1
        self.lastPlus += 1
        #self.printBoard()
        self.placeAtom(index)
        self.updateBoard(self.getIndex(index))
        return len(self.board) >= 19 

    def start(self):
        self.initBoard()
        while  len(self.board) < 19:
            self.movecount += 1
            self.lastPlus += 1
            self.printBoard()
            index = int(input("$> "))
            self.placeAtom(index)
            self.updateBoard(self.getIndex(index))
        self.score = self.score + sum((map(lambda x: 0 if x < 0 else x, self.board)))
        print("score: ", self.score)


    def printBoard(self):
        print("Score: ", self.score)
        print("Next Atom: ", self.elemStr(self.nextElem))
        for i in range(min(len(self.board), 19)):
            print("[%02d]    "% (self.board[i]), end = '')
        print("\n")
        for i in range(max(min(len(self.board), 19), 1)): 
            print("    (%02d)"% (i+1), end = '')
        print("\n")


    def initBoard(self):
        self.board = []
        self.score = 0
        for i in range(1, 6):
            self.board.insert(-1, randint(1, 3));
        
        self.nextElem = randint(1, 4);
        ##
        #board = [2, 1, 2, 3, 1, 1, 3, 2, 1, 2, 1]
        #nextElem = PLUSATOM

    def placeAtom(self, location):
        location = max(min(int(location),len(self.board)), 1)
        self.board.insert(location, self.nextElem)

        self.nextElem = self.rollNextElem()

        
        #todo: tweak settings; add new atom generation range; amazon aws
    def rollNextElem(self):
        if(self.lastPlus >= 5):
            self.lastPlus = 0
            return -1
        if self.score > 750:
            if(randint(1, 80) == 1):
                return self.BLACKPLUS
        mn = min(min([x for x in self.board if x > 0]),1)
        mx = max(max(self.board),1)

        #dist = gauss(0, 1)
        #dist = (dist+1)/2
        return int(mn + random()*(mx-mn)+1)
        #if r == 0:
        #    print(f"min : {mn} \n mddddax: {mx} \n d: {dist}\n r: {r}")
        #return r

    def updateBoard(self, lastIndex, chain = False, depth = 1):
        iterator = lastIndex
        #print(board)
        #print("--")

        lval = self.board[self.getIndex(lastIndex - 1)]
        rval = self.board[self.getIndex(lastIndex + 1)]
        cval = self.board[lastIndex]




        if cval == self.PLUSATOM:
            newVal = lval + 1
            cval = lval
        else:
        
            if chain:
                if lval < cval:
                    newVal = cval + 1
                else:
                    newVal = lval + 2
            else:
                if cval == self.BLACKPLUS:
                    newVal = max(lval, rval) + 3
                    lval = 0
                    rval = 0
                else:
                    return

        if( lval == rval and len(self.board) >= 3):
            iterator = iterator - 1
            self.board.pop(self.getIndex(iterator))
            self.board[iterator] = newVal
            iterator = self.getIndex(iterator + 1)
            self.board.pop(iterator)

            M = 1 + (.5 * depth)
            B = 2 * M *  (lval - cval + 1)
            if(depth == 1) :
                self.score += floor(1.5*(cval + 1))
            else:
                if lval < cval:
                    self.score += (floor(M*(cval+1)))
                else:
                    self.score += (floor(M*(cval+1))) + B

            #print(score)
            self.updateBoard(self.getIndex(iterator - 1), True, depth+1)

    def getIndex(self, i):
        if len(self.board) == 0:
            return 0
        return int(i%len(self.board))

    def elemStr(self, i):
        if(i == self.PLUSATOM):
            return "Plus"
        else:
            return str(i)

if __name__ == "__main__":
    game = Game() 
    game.start()
