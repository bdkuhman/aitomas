import neat
import os
import multiprocessing
import pickle
from neat import checkpoint
import Game
import visualize
from os import path
from numpy.random import choice 


runs_per_net = 5
FILE = "neat-checkpoint-1123o"
def eval_genome(genome, config):
    net = neat.nn.RecurrentNetwork.create(genome, config)
    fitnesses = []

    for runs in range(runs_per_net):
        game = Game.Game()
        net.reset()

        over = False
        #run until over
        while(not over):
            list1 = [float(0)]*19
            list2 = game.board
            for i in range(len(game.board)):
                list1[i] = float(game.board[i])
            list1[-1] = game.nextElem; 
            inputs = list1
            outputs = net.activate(inputs)
            ##print(max(action))
            action = choice(range(0, 18), 1, p=[x/sum(outputs) for x in outputs])
            over = game.step(action)
        game.score += sum([x for x in game.board if x > 0])
        game.score /= game.movecount
        game.score += game.movecount
        if(game.movecount <= 14):
            game.score = -1
        #print(game.score, game.movecount)
        fitnesses.append(game.score)
        #print(game.score, game.board)
    return min(fitnesses)




def run():
    # Load the config file, which is assumed to live in
    # the same directory as this script.
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'config-ctrnn')
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_path)
    chk = checkpoint.Checkpointer(1000, 900)

    #if(os._exists(FILE)):
    pop = neat.Population(config)

    if os.path.exists(FILE):
        pop = chk.restore_checkpoint(FILE)
        print("restoring file:", FILE)
    
    stats = neat.StatisticsReporter()
    pop.add_reporter(stats)
    pop.add_reporter(neat.StdOutReporter(True))
    pop.add_reporter(chk)
    pe = neat.ParallelEvaluator(multiprocessing.cpu_count(), eval_genome)
    #ev = neat.ThreadedEvaluator(8, eval_genome)
    winner = pop.run(pe.evaluate)

    # Save the winner.
    with open('winner-ctrnn', 'wb') as f:
        pickle.dump(winner, f)

    print(winner)

    visualize.plot_stats(stats, ylog=True, view=True, filename="ctrnn-fitness.svg")
    visualize.plot_species(stats, view=True, filename="ctrnn-speciation.svg")

    node_names = {-1: 'x', -2: 'dx', -3: 'theta', -4: 'dtheta', 0: 'control'}
    visualize.draw_net(config, winner, True, node_names=node_names)

    visualize.draw_net(config, winner, view=True, node_names=node_names,
                       filename="winner-ctrnn.gv")
    visualize.draw_net(config, winner, view=True, node_names=node_names,
                       filename="winner-ctrnn-enabled.gv", show_disabled=False)
    visualize.draw_net(config, winner, view=True, node_names=node_names,
                       filename="winner-ctrnn-enabled-pruned.gv", show_disabled=False, prune_unused=True)


if __name__ == '__main__':
    run()
